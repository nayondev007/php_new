<?php
// Start or resume the session
session_start();

// Check if the user is already logged in
if (isset($_SESSION['username'])) {
    header('Location: profile.php'); // Redirect to profile page if already logged in
    exit();
}

// Initialize variables
$username = '';
$error_message = '';

// Check if the signup form is submitted
if (isset($_POST['signup'])) {
    // Retrieve form data
    $username = $_POST['username'];
    $password = $_POST['password'];

    // TODO: Validate form data and perform necessary checks (e.g., password strength)

    // Hash the password before storing
    $hashed_password = password_hash($password, PASSWORD_DEFAULT);

    // TODO: Insert the user data into the database

    // For this example, let's assume successful registration
    $_SESSION['username'] = $username; // Log in the user automatically
    header('Location: profile.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.5.2/dist/css/bootstrap.min.css">
</head>

<body>
    <div class="container mt-5">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Sign Up</h2>
                <form method="post" action="signup.php">
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" name="username" value="<?php echo $username; ?>" required>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control" id="password" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary" name="signup">Sign Up</button>
                    <?php if ($error_message !== '') : ?>
                        <p class="text-danger mt-2"><?php echo $error_message; ?></p>
                    <?php endif; ?>
                </form>
                <p class="mt-3">Already have an account? <a href="index.php">Login here</a></p>
            </div>
        </div>
    </div>
</body>

</html>